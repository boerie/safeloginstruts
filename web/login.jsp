<%-- 
    Document   : login
    Created on : Jan 26, 2015, 10:43:09 AM
    Author     : burakkelebek
--%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="stylesheet.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Safe Login App</title>
    </head>
    <body>
        <h1>Login Form!</h1>
        <p>If log in is successful , you will be redirected to a super secret page...  </p>
        <html:form action="/login">
            <table border="0">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <bean:write name="LoginForm" property="error" filter="false"/>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>Enter your username:</td>
                        <td><html:text property="username" /></td>
                    </tr>
                    <tr>
                        <td>Enter your password:</td>
                        <td><html:password property="password" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><html:submit value="Login" /></td>
                    </tr>
                </tbody>
            </table>
        </html:form>
    </body>
</html>
