<%-- 
    Document   : success
    Created on : Jan 26, 2015, 10:46:19 AM
    Author     : burakkelebek
--%>
<%@page import="com.myapp.struts.UpdateForm"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="stylesheet.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Success</title>
    </head>
    <body>
        <h1>Congratulations!</h1>

        <p>You have successfully logged in.</p>
        
        <form action ="login.do" method="get">
            <input type="submit" value="logout">
        </form>
        
        <p>This page is so secure, that you have to provide your credentials again before you can update your age!</p>
        <html:form action="/success">
            <table border="0">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <bean:write name="UpdateForm" property="error" filter="false"/>
                            <bean:write name="UpdateForm" property="error2" filter="false"/>
                            <bean:write name="UpdateForm" property="error3" filter="false"/>
                            <bean:write name="UpdateForm" property="OKmessage" filter="false"/>
                            &nbsp;</td>
                    </tr>
                      <tr>
                        <td>Enter your username:</td>
                        <td><html:text property="username" /></td>
                    </tr>
                    <tr>
                        <td>Enter your password:</td>
                        <td><html:password property="password" /></td>
                    </tr>
                    <tr>
                        <td>Change your age:</td>
                        <td><html:text property="name" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><html:submit value="update" /></td>
                    </tr>
                </tbody>
            </table>
        </html:form>
        <td>Your new age is: <bean:write name="UpdateForm" property="age" />

    </body>
    
    
                
</html>
