/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author burakkelebek
 */
public class UpdateAction extends org.apache.struts.action.Action {
    
    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private final static String FAILURE = "failure";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
       
        // extract user data
        UpdateForm formBean = (UpdateForm) form;
        
        DbDAO d = new DbDAO();
        String name = formBean.getUsername();
        String pass = formBean.getPassword();
        String age = formBean.getName();
        // perform validation
        if ((name.equals("")  && age.equals("") && pass.equals(""))) {
            formBean.setError3();
            return mapping.findForward(FAILURE);
        }
                
        
        if (!formBean.isNumeric(age) // age parameter is not numeric
                || age == null || // age parameter does not exist
                age.equals("")) // age parameter is empty
        {
            formBean.setError();
            return mapping.findForward(FAILURE);
        }
        if (d.checkLogin(name, pass)) {
            formBean.setAge(d.update(age, name));
            formBean.setOKmessage();
            return mapping.findForward(SUCCESS);
        } else {
            formBean.setError2();
            return mapping.findForward(FAILURE);
        }
    }
}
