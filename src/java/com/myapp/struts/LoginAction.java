/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpSession;
import org.apache.struts.chain.contexts.ServletActionContext;
import javax.servlet.SessionCookieConfig;

/**
 *
 * @author burakkelebek
 */
public class LoginAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private final static String FAILURE = "failure";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

//            LoginForm lf = (LoginForm) form;
//            String name = lf.getUsername();
//            String pass = lf.getPassword();
//            DB d = new DB();
//            d.insert(name, pass);
//            d.select(name);
//            return mapping.findForward(SUCCESS);
        LoginForm lf = (LoginForm) form;
        String name = lf.getUsername();
        String pass = lf.getPassword();
        DbDAO d = new DbDAO();
        if (d.checkLogin(name, pass)) {
            return mapping.findForward(SUCCESS);
        } else {
            lf.setError();
            return mapping.findForward(FAILURE);
        }
    }
}
