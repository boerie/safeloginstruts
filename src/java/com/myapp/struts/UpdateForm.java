/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author burakkelebek
 */
public class UpdateForm extends org.apache.struts.action.ActionForm {
    
    public String name;
    private String error;
    private String error2;
    private String error3;
    private String OKmessage;
    public String age;
    
    public static boolean isNumeric(String str) {
        return str.matches("[+-]?\\d*(\\.\\d+)?");
    }

    public String getError3() {
        return error3;
    }

    public void setError3() {
        this.error3 = "<span style='color:red'>Don't leave the form empty please :)</span>";;
    }

    public String getOKmessage() {
        return OKmessage;
    }

    public void setOKmessage() {
        this.OKmessage = "<span style='color:green'>New age updated! Wow, is that your real age? You look much younger! :)</span>";
    }

    public String getError2() {
        return error2;
    }

    public void setError2() {
        this.error2 = "<span style='color:red'>Username or password is wrong :(</span>";
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    public String getError() {
        return error;
    }

    public void setError() {
        this.error = "<span style='color:red'>Please provide valid entry for age</span>";
    }
    
    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     *
     */
    public UpdateForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getName() == null || getName().length() < 1) {
            errors.add("name", new ActionMessage("error.name.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        return errors;
    }
    
    
    // username & password
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    private String password;
    
    
}
