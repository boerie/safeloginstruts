/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts;

/**
 *
 * @author burakkelebek
 */
public class LoginForm extends org.apache.struts.action.ActionForm {
    
    private String age;
    private String username;
    private String error;
    private String password;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getError() {
        return error;
    }

    public void setError() {
        this.error = "<span style='color:red'>Username or password is wrong!! I'm not telling which one :)</span>";
    }

    /**
     *
     */
    public LoginForm() {
        super();
        // TODO Auto-generated constructor stub
    }
}
