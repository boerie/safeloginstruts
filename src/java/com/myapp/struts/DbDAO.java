/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts;

import java.io.Serializable;
import java.sql.*;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author burakkelebek
 */
public class DbDAO implements Serializable {
    private Connection con;
    
    public DbDAO(){
        createConnection();
    }
    
    public Connection getConnection(){
        return con;
    }
    
    public void createConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/struts?zeroDateTimeBehavior=convertToNull";
            String user="root";
            String pass="";
            con=DriverManager.getConnection(url,user,pass);
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public void closeConnection(){
        try{
            con.close();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public void insert(String name, String pass) {
        try {
            String sql = "INSERT INTO login VALUES(?,?)";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, name);
            pst.setString(2, pass);

            pst.executeUpdate();
            System.out.println("Record entered succesfully");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public String update(String age, String name){
        try {
            String sql = "UPDATE login set age = ? where name = ?";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, age);
            pst.setString(2, name);
            pst.executeUpdate();
            System.out.println("Record updated succesfully");
        } catch (Exception e) {
            System.out.println(e);
        }
        return age;
    }
    
    public String select(String name) {
        String age = null;
        try {
            String sql = "SELECT * FROM login WHERE name = ?";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, name);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                age = rs.getString("age");
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return age;
    }

    public Boolean checkLogin(String name, String pass) {
        Boolean loginFlag = false;
        try {
            pass = AeSimpleSHA1.SHA1(pass);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DbDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(DbDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            String sql = "SELECT NAME, PASS FROM LOGIN WHERE NAME=? AND PASS=?";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2, pass);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                loginFlag = true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return loginFlag;
    }
    
}
